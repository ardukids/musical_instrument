int pinButton1 = 8;
int pinButton2 = 9;
int pinButton3 = 10;
int pinBuzzer = 11;

void setup() 
{
  pinMode(pinButton1, INPUT_PULLUP);
  pinMode(pinButton2, INPUT_PULLUP);
  pinMode(pinButton3, INPUT_PULLUP);
  pinMode(pinBuzzer, OUTPUT);
}

void loop() 
{
  //read each button state
  int btn1 = digitalRead(pinButton1);
  int btn2 = digitalRead(pinButton2);
  int btn3 = digitalRead(pinButton3);

  //INPUT_PULLUP uses inverted logic
  if (btn1 == LOW)
  {
    //play C arpeggio
    tone(pinBuzzer, 261.62);
    delay(333);
    tone(pinBuzzer, 329.62);
    delay(333);
    tone(pinBuzzer, 391.99);
    delay(333);
  }
  else if (btn2 == LOW)
  {
    //play F arpeggio
    tone(pinBuzzer, 261.62);
    delay(333);
    tone(pinBuzzer, 349.22);
    delay(333);
    tone(pinBuzzer, 440.00);
    delay(333);
  }
  else if (btn3 == LOW)
  {
    //play G arpegio
    tone(pinBuzzer, 246.94);
    delay(333);
    tone(pinBuzzer, 293.66);
    delay(333);
    tone(pinBuzzer, 399.99);
    delay(333);
  }
  else
  {
    //stop all sounds
    noTone(pinBuzzer);
  }
}
